fn day_1_part_1() {
    let input = include_str!("day_1.txt");
    let mut sum = 0;
    for line in input.lines() {
        let digits = line
            .chars()
            .filter(|c| c.is_digit(10))
            .collect::<Vec<_>>();

        let mut string = String::new();
        string.push_str(&digits.iter().next().unwrap().to_string());
        string.push_str(&digits.iter().last().unwrap().to_string());

        println!("{}", string);
        let string_as_int = string.parse::<u32>().unwrap();
        sum += string_as_int;
    }
    println!("Part 1: {}", sum);
}

struct Symbol {
    names: Vec<String>,
    value: String
}

fn day_1_part_2() {
    let input = include_str!("day_1.txt");
    let mut sum = 0;

    let symbols = vec![
        Symbol { names: vec!["one".to_string(), "1".to_string()], value: "1".to_string() },
        Symbol { names: vec!["two".to_string(), "2".to_string()], value: "2".to_string() },
        Symbol { names: vec!["three".to_string(), "3".to_string()], value: "3".to_string() },
        Symbol { names: vec!["four".to_string(), "4".to_string()], value: "4".to_string() },
        Symbol { names: vec!["five".to_string(), "5".to_string()], value: "5".to_string() },
        Symbol { names: vec!["six".to_string(), "6".to_string()], value: "6".to_string() },
        Symbol { names: vec!["seven".to_string(), "7".to_string()], value: "7".to_string() },
        Symbol { names: vec!["eight".to_string(), "8".to_string()], value: "8".to_string() },
        Symbol { names: vec!["nine".to_string(), "9".to_string()], value: "9".to_string() },
    ];

    for line in input.lines() {
        println!("{}", line);
        let mut symbol_positions: Vec<(usize, &Symbol)> = Vec::new();

        for symbol in symbols.iter() {
            for name in symbol.names.iter() {
                let mut symbol_name_positions: Vec<(usize, &Symbol)>  = line
                    .match_indices(name)
                    .map(|(i, _)| (i, symbol))
                    .collect();

                symbol_positions.append(&mut symbol_name_positions);
            }
        }

        symbol_positions.sort_by(|a, b| a.0.cmp(&b.0));

        let mut string = String::new();
        string.push_str(&symbol_positions.iter().next().unwrap().1.value.to_string());
        string.push_str(&symbol_positions.iter().last().unwrap().1.value.to_string());

        println!("{}", string);
        let string_as_int = string.parse::<u32>().unwrap();
        sum += string_as_int;
    }
    println!("Part 2: {}", sum);

}

fn main() {
    day_1_part_1();
    day_1_part_2();
}
