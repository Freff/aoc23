use std::collections::HashMap;
use std::convert::TryInto;

struct Series {
    games: Vec<Game>
}

impl Series {
    pub fn new(series_file: String) -> Series {
        let games: Vec<Game> = series_file
            .lines()
            .map(|x| Game::new(x.to_string()))
            .collect();

        let series = Series {
            games: games,
        };

        return series;
    }

    pub fn get_score_of_series_part_1(&self) -> i32 {
        return self.games.iter().fold(0, |acc, x| acc + x.score);
    }

    pub fn get_won_scratchcards_in_series_part_2(&self) -> i32 {
        let mut scratchcard_victories: HashMap<i32, i32> = HashMap::new();

        //pre-populate map with 1 values
        for game in self.games.iter() {
            scratchcard_victories.insert(game.card_number, 1);
        }

        // this should work because by the time we get to a game we have already worked out the
        // score for all previous games and should have all information
        for (pos, game) in self.games.iter().enumerate() {
            let num_downstream_games = game.card_number + game.number_of_matches;

            for n in game.card_number+1..num_downstream_games+1 {
                let downstream_victories = *scratchcard_victories.get(&n).unwrap();
                let new_total = downstream_victories + (1*scratchcard_victories.get(&game.card_number).unwrap());
                scratchcard_victories.insert(n, new_total);
            }
        }

        let count = scratchcard_victories.values().fold(0, |acc, x| acc + x);

        return count;
    }
}

struct Game {
    card_number: i32,
    score: i32,
    my_numbers: Vec<i32>,
    winning_numbers: Vec<i32>,
    number_of_matches: i32
}

impl Game {
    pub fn new(game_entry: String) -> Game {
        let game_parts: Vec<&str> = str::split(&game_entry, ":").collect();
        let card_number = str::split(&game_parts[0], " ")
                               .filter(|x| !x.is_empty())
                               .collect::<Vec<&str>>()[1]
                               .parse::<i32>()
                               .unwrap();

        let game_numbers = str::split(&game_parts[1], "|")
                                      .collect::<Vec<&str>>();

        let my_numbers: Vec<i32> = Game::get_numbers_from_game(game_numbers[0]);
        let winning_numbers: Vec<i32> = Game::get_numbers_from_game(game_numbers[1]);

        let mut game = Game {
            card_number: card_number,
            score: 0,
            number_of_matches: 0,
            my_numbers: my_numbers,
            winning_numbers: winning_numbers,
        };

        // Y I K E
        game.score = game.calculate_score_part_1().0;
        game.number_of_matches = game.calculate_score_part_1().1;

        return game;
    }

    fn get_numbers_from_game(numbers_as_str: &str) -> Vec<i32> {
        str::split(numbers_as_str, " ")
            .collect::<Vec<&str>>()
            .iter()
            .filter(|x| !x.is_empty())
            .map(|x| x.parse::<i32>().unwrap())
            .collect()
    }

    fn calculate_score_part_1(&self) -> (i32, i32) {
        let mut score = 0;
        let mut num_matches = 0;
        for number in self.my_numbers.iter() {
            if self.winning_numbers.contains(number) {
                if score == 0 {
                    score = 1;
                } else {
                    score *= 2;
                }
                num_matches += 1;
            }
        }

        return (score, num_matches);
    }
}

fn main () {
    let input = include_str!("input.txt");

    let series = Series::new(input.to_string());

    println!("Score part 1: {}", series.get_score_of_series_part_1());

    println!("Score part 2: {}", series.get_won_scratchcards_in_series_part_2());
}

mod tests {

    #[test]
    fn test_game() {
        let game = Game::new("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53".to_string());
        assert_eq!(game.my_numbers, vec![41, 48, 83, 86, 17]);
        assert_eq!(game.winning_numbers, vec![83, 86,  6, 31, 17,  9, 48, 53]);
        assert_eq!(game.score, 8);
        assert_eq!(game.card_number, 1);
    }

    #[test]
    fn test_get_game_numbers() {
        let game_numbers = Game::get_numbers_from_game("41 48 83 86 17");
        assert_eq!(game_numbers, vec![41, 48, 83, 86, 17]);

        let game_numbers = Game::get_numbers_from_game(" 41 48 83 86 17" );
        assert_eq!(game_numbers, vec![41, 48, 83, 86, 17]);
    }
}
